window.onload = init;

function init() {
	var menuButton = document.getElementsByClassName('menu-button')[0];
	var menu = document.getElementsByClassName('menu-expanded')[0];
	var header = document.getElementsByClassName('main-nav')[0];
	var logoText = document.getElementsByClassName('logo-text')[0];

menuButton.addEventListener('click', function(e){
	e.preventDefault();
	(menuButton.classList.contains('active') === true) ? menuButton.classList.remove('active'): menuButton.classList.add('active');

	if(menuButton.classList.contains("active")){
			menu.style.display = "block";
		} else {
			menu.style.display = "none";
		}

});

/*Get the VH of the viewport*/
	var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
	console.log(h);
/*Fixed header*/
	function stickyScroll(e) {
		if (window.pageYOffset > h) {
			header.classList.add('fixed');
			logoText.style.visibility = "visible";
		}

		if (window.pageYOffset < h) {
			header.classList.remove('fixed');
			logoText.style.visibility = "hidden";

		}


	}

	/*Listen for scroll event and fire stickyscroll function*/
	window.addEventListener('scroll', stickyScroll, false);












}

$(document).ready(function(){
	console.log('ready');

	$('.about-link-dt, .go-to-about').on('click', function(e){
		e.preventDefault();
		$('html, body').animate({
			scrollTop: $('.about').offset().top
		}, 500);


	});

	// $('.testimonial-left').on('click', function(e){
	// 	$('.testimonial').addClass('move-left');
	// });
	// 
	var testimonial = $('.testimonial-wrap');
	var testimonialCount = testimonial.children().length;
	var wrapWidth = testimonialCount * testimonial;
	testimonial.css('width', wrapWidth);
	var animTime = 550;
	console.log(testimonialCount);
	// console.log(wrapWidth);

	$('.testimonial-left').on('click', function(e){
		var last = $('.testimonial-content').eq(2);
		console.log(last);
		last.removeClass('fade-out');
		last.css({'margin-left':'-100%'}).addClass('fade-in');
		$('.testimonial-content').eq(0).before(last);
		last.animate({'margin-left': '0px'}, animTime);
	});

		$('.testimonial-right').on('click', function(e){
		var first = $('.testimonial-content').eq(0);
		
		
		
		first.animate({'margin-left': '-100%'}, animTime, function(){
			first.css({'margin-left':'0px'});
			$('.testimonial-content').eq(2).after(first);
		});
	});

		/*rotation of testimonials*/
var rotation = true;
var speed = 3800;
var rotationTime = setInterval(rotationTestimonials, speed);
		$('.testimonial').on({
			mouseenter:function(){
				console.log('entered');
				rotation = false;
			},
			mouseleave: function(){
				console.log('left!');
				rotation = true;
			}
		});

		function rotationTestimonials(speed){
			if(rotation != false){
				var first = $('.testimonial-content').eq(0);
				first.animate({'margin-left': '-100%'}, 750, function(){
					first.css({'margin-left': '0px'});
					$('.testimonial-content').eq(2).after(first);
				});
			}
		}

// 		/*Google map usability enhancement*/
// 	$('#map').click(function () {
//     $('#map').css("pointer-events", "auto");
// });

// $( "#map" ).mouseleave(function() {
//   $('#map').css("pointer-events", "none"); 
// });



});